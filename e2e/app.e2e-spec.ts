import { EthDashboardPage } from './app.po';

describe('eth-dashboard App', () => {
  let page: EthDashboardPage;

  beforeEach(() => {
    page = new EthDashboardPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
