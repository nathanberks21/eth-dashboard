import { TestBed, inject } from '@angular/core/testing';

import { EthDataService } from './eth-data.service';

describe('EthDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EthDataService]
    });
  });

  it('should ...', inject([EthDataService], (service: EthDataService) => {
    expect(service).toBeTruthy();
  }));
});
