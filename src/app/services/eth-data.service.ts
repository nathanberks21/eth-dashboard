import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import {Observable} from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class EthDataService {

  constructor(private http: Http) { }

  private ethDataUrl = '/api/getEthData';

  getData() : Observable<any[]> {
         return this.http.get(this.ethDataUrl)
          .map((res:Response) => res.json().data)
          .catch((error:any) => Observable.throw(error.json() || 'Server error'));
     }

}
