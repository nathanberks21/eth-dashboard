import { Component, OnInit } from '@angular/core';

import { EthDataService } from '../services/eth-data.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  hashPower: number;
  ethInfo: any;
  data: any;

  constructor(private ethDataService: EthDataService) {
    this.hashPower = 150;
    this.ethInfo = [];
  }

  ngOnInit() {
    this.ethDataService.getData()
      .subscribe(data => {
        this.data = data;
        this.data.sort((a, b) => {
          if (a.time < b.time) {
              return -1
          } else if (b.time < a.time) {
              return 1
          } else {
              return 0
          }
        })
        this.updateData();
      },
      err => {
        console.log(`An error occurred ${err}`);
      })
  }

  updateData = () => {
    this.ethInfo = [];
    for (let i = 0; i < this.data.length; i++) {
      const d = this.data[i];
      const ethPerDay = this.calcEthPerMin(d);
      d.networkHashNorm = d.networkHash / 1e9;
      const entry = Object.assign(d, {ethPerDay});

      if (i === 0) {
        entry.change = 0;
      } else {
        const prev = this.data[i - 1].ethPerDay;
        entry.change = ((ethPerDay - prev) / ethPerDay);
      }

      this.ethInfo.push(entry);
      
    }
  }

  calcEthPerMin = (d) => {
    const ratio = this.hashPower * 1e6 / d.networkHash;
    const blocksPerMin = 60.0 / d.blockTime;
    // 5 eth per block found
    const ethPerMin = blocksPerMin * 5.0;
    // 1440 minutes in a day
    const daily = ratio * ethPerMin * 1440
    return daily;
  };

}
